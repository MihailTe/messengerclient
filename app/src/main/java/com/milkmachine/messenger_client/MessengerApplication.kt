package com.milkmachine.messenger_client

import android.app.Application
import android.content.Context

/**
 * Created by kefir on 03.03.2017.
 */
class MessengerApplication : Application() {

    companion object {
        lateinit var context: Context
            private set
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
    }
}