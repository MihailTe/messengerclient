package com.milkmachine.messenger_client.auth

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.telephony.SmsMessage
import android.view.View.VISIBLE
import android.widget.Button
import com.milkmachine.messenger_client.R
import com.milkmachine.messenger_client.chat.ChatActivity
import com.milkmachine.messenger_client.core.DataManager
import com.milkmachine.messenger_client.core.DataManager.CORRECT_CODE_LENGTH
import com.milkmachine.messenger_client.core.DataManager.CORRECT_PHONE_LENGTH
import com.milkmachine.messenger_client.core.DataManager.showMessage
import com.milkmachine.messenger_client.core.receivers.sms.SmsConsumer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.slots.PredefinedSlots
import ru.tinkoff.decoro.watchers.MaskFormatWatcher

@RuntimePermissions
class AuthActivity : AppCompatActivity(), SmsConsumer {

    private lateinit var phoneField: TextInputEditText
    private lateinit var userNameField: TextInputEditText
    private lateinit var codeField: TextInputEditText
    private lateinit var codeWrapper: TextInputLayout
    private lateinit var userNameWrapper: TextInputLayout
    private lateinit var phoneWrapper: TextInputLayout
    private lateinit var button: Button
    private lateinit var toolbar: Toolbar

    private lateinit var userName: String

    private var viewState: States = States.INIT_STATE

    private enum class States {
        INIT_STATE, CODE_RECEIVING_STATE
    }

    companion object {
        const val USER_NAME = "USER_NAME"
    }

    //region ============================== Lifecycle ==============================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        setUpViews()
        initListeners()
        initPhoneEditText()

        setSupportActionBar(toolbar)
    }

    override fun onResume() {
        super.onResume()
        AuthActivityPermissionsDispatcher.requestSmsReceivePermissionWithCheck(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isFinishing) DataManager.unbindSmsConsumer()
    }

    //endregion


    private fun setUpViews() {
        userNameField = findViewById(R.id.user_name_field) as TextInputEditText
        phoneField = findViewById(R.id.phone_field) as TextInputEditText
        codeField = findViewById(R.id.code_field) as TextInputEditText
        codeWrapper = findViewById(R.id.code_field_wrapper) as TextInputLayout
        userNameWrapper = findViewById(R.id.user_name_field_wrapper) as TextInputLayout
        phoneWrapper = findViewById(R.id.phone_field_wrapper) as TextInputLayout
        button = findViewById(R.id.get_code_button) as Button
        toolbar = findViewById(R.id.toolbar) as Toolbar
    }

    private fun initListeners() {
        button.setOnClickListener {
            when (viewState) {
                States.INIT_STATE -> requestSmsCode()
                States.CODE_RECEIVING_STATE -> validateSmsCode()
            }
        }
    }

    private fun initPhoneEditText() {
        val mask = MaskImpl.createTerminated(PredefinedSlots.RUS_PHONE_NUMBER)
        val watcher = MaskFormatWatcher(mask)
        watcher.installOn(phoneField)
    }

    private fun requestSmsCode() {
        val phone = getUserPhone()

        if (phone.length != CORRECT_PHONE_LENGTH) {
            showMessage("Телефон введен неверно")
            return
        }

        DataManager.requestCodeBySms(phone)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showCodeReceivingState()
                    DataManager.bindSmsConsumer(this)
                }, { DataManager.onFatalError(it) })
    }

    private fun showCodeReceivingState() {
        userNameWrapper.setActive(false)
        userName = userNameField.text.toString()

        phoneWrapper.setActive(false)

        codeWrapper.visibility = VISIBLE

        button.setText(android.R.string.ok)

        viewState = States.CODE_RECEIVING_STATE
    }

    private fun TextInputLayout.setActive(isActive: Boolean) {
        isEnabled = isActive
        isSelected = isActive
    }

    private fun validateSmsCode() {
        val code = "${codeField.text}"

        fun showIncorrectCodeMessage() = showMessage("Неправильный код")

        if (code.length != CORRECT_CODE_LENGTH) {
            showIncorrectCodeMessage()
            return
        }

        DataManager.checkSmsCode(getUserPhone(), code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.code() == 202) showChatActivity()
                    else showIncorrectCodeMessage()
                }, { DataManager.onFatalError(it) })
    }

    override fun consumeSms(smsMessage: SmsMessage) {

        if (smsMessage.originatingAddress == "SMS.RU") {
            codeField.setText(smsMessage.messageBody)
            codeField.isEnabled = false
            validateSmsCode()
        }
    }

    private fun getUserPhone() = "${phoneField.text}".filter(Char::isDigit)

    private fun showChatActivity() {
        val intent = Intent(this, ChatActivity::class.java)
        intent.putExtra(USER_NAME, userName)
        startActivity(intent)
        finish()
    }

    //region ============================== Permissions ==============================

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        AuthActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults)
    }

    @NeedsPermission(Manifest.permission.RECEIVE_SMS)
    fun requestSmsReceivePermission() = Unit

    //endregion
}
