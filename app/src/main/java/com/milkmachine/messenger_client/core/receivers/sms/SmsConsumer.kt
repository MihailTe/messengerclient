package com.milkmachine.messenger_client.core.receivers.sms

import android.telephony.SmsMessage

/**
 * Created by kefir on 07.03.2017.
 */
interface SmsConsumer {

    fun consumeSms(smsMessage: SmsMessage)
}