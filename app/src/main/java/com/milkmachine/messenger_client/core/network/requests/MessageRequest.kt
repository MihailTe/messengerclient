package com.milkmachine.messenger_client.core.network.requests

import com.milkmachine.messenger_client.core.entitites.Message

/**
 * Created by kefir on 09.03.2017.
 */
data class MessageRequest(val text: String, val userName: String) {

    constructor(message: Message) : this(text = message.text, userName = message.username)
}