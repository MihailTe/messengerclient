package com.milkmachine.messenger_client.core.entitites

import com.milkmachine.messenger_client.core.network.requests.MessageRequest

/**
 * Created by kefir on 08.03.2017.
 */
data class Message(val text: String,
                   val username: String) {

    constructor(messageRequest: MessageRequest)
            : this(text = messageRequest.text, username = messageRequest.userName)
}