package com.milkmachine.messenger_client.core.network.requests

import com.squareup.moshi.Json

/**
 * Created by kefir on 07.03.2017.
 */
data class PhoneRequest(@Json(name = "phone") val value: String)