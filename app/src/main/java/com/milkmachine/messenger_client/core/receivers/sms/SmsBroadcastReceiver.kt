package com.milkmachine.messenger_client.core.receivers.sms

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.SmsMessage
import com.milkmachine.messenger_client.core.DataManager


/**
 * Created by kefir on 07.03.2017.
 */
class SmsBroadcastReceiver : BroadcastReceiver() {

    companion object {
        const val ACTION = "android.provider.Telephony.SMS_RECEIVED"
    }

    override fun onReceive(context: Context, intent: Intent?) {
        val bundle = intent?.extras

        if (intent?.action == ACTION && bundle != null) {

            val pdus = bundle.get("pdus") as Array<*>

            @Suppress("DEPRECATION")
            val messages = pdus.map { SmsMessage.createFromPdu(it as ByteArray) }

            messages.forEach { DataManager.smsConsumer?.consumeSms(it) }
        }
    }
}