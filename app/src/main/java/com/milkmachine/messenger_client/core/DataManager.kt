package com.milkmachine.messenger_client.core

import android.widget.Toast
import com.milkmachine.messenger_client.MessengerApplication
import com.milkmachine.messenger_client.core.entitites.Message
import com.milkmachine.messenger_client.core.network.requests.AuthDataRequest
import com.milkmachine.messenger_client.core.network.requests.MessageRequest
import com.milkmachine.messenger_client.core.network.requests.PhoneRequest
import com.milkmachine.messenger_client.core.network.rest.AuthRestService
import com.milkmachine.messenger_client.core.network.rest.createService
import com.milkmachine.messenger_client.core.network.ws.WebSocketManager
import com.milkmachine.messenger_client.core.receivers.sms.SmsConsumer
import com.milkmachine.messenger_client.core.utils.fromJson
import io.reactivex.Observable

/**
 * Created by kefir on 03.03.2017.
 */
object DataManager {

    const val CORRECT_CODE_LENGTH = 5
    const val CORRECT_PHONE_LENGTH = 11

    private val appContext = MessengerApplication.context
    private val webSocketManager = WebSocketManager()
    private val restService = createService(AuthRestService::class.java)

    var smsConsumer: SmsConsumer? = null
        private set

    fun onFatalError(e: Throwable) {
        e.printStackTrace()
        showMessage("Произошла непредвиденная ошибка, попробуйте позже")
    }

    fun showMessage(message: String) {
        Toast.makeText(appContext, message, Toast.LENGTH_LONG).show()
    }

    //region ============================== SmsReceiver ==============================

    fun bindSmsConsumer(consumer: SmsConsumer) {
        smsConsumer = consumer
    }

    fun unbindSmsConsumer() {
        smsConsumer = null
    }

    //endregion

    //region ============================== Authorization ==============================

    fun requestCodeBySms(phone: String) = restService.requestCodeBySms(PhoneRequest(phone))

    fun checkSmsCode(phone: String, code: String) =
            restService.validateSmsCode(AuthDataRequest(phone, code))

    //endregion

    //region ============================== Chat ==============================

    fun getChatObservable(): Observable<Message> {
        return webSocketManager.getChatObservable().map {
            val response = it.payload.fromJson(MessageRequest::class.java)
            Message(response)
        }
    }

    fun getAllMessages(): List<Message> {
        return arrayListOf()
    }

    fun sendMessage(message: Message) {
        webSocketManager.sendMessage(MessageRequest(message))
    }

    fun onUnsubscribeFromChat() {
        webSocketManager.unsubscribeFromChat()
    }

    //endregion
}