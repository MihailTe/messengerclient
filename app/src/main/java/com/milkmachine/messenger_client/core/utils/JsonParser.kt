package com.milkmachine.messenger_client.core.utils

import com.squareup.moshi.Moshi

/**
 * Created by kefir on 09.03.2017.
 */

fun <T> String.fromJson(clazz: Class<T>): T = MoshiGenerator.moshiInstance.adapter(clazz).fromJson(this)

fun <T : Any> T.toJson(): String {
    return MoshiGenerator.moshiInstance.adapter<T>(this::class.java).toJson(this)
}

private object MoshiGenerator {
    val moshiInstance: Moshi by lazy { Moshi.Builder().build() }
}