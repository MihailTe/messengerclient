package com.milkmachine.messenger_client.core.network.rest

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.milkmachine.messenger_client.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Created by kefir on 03.03.2017.
 */

private const val REST_API_ENDPOINT = "http://${BuildConfig.SERVER_ADDRESS}/api/"

private val retrofitBuilder = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl(REST_API_ENDPOINT)

fun <S> createService(clazz: Class<S>): S {
    return retrofitBuilder.client(createClient())
            .build()
            .create(clazz)
}

private fun createClient(): OkHttpClient {
    return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
}