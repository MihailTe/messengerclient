package com.milkmachine.messenger_client.core.network.rest

import com.milkmachine.messenger_client.core.network.requests.AuthDataRequest
import com.milkmachine.messenger_client.core.network.requests.PhoneRequest
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by kefir on 03.03.2017.
 */
interface AuthRestService {

    @POST("auth")
    fun requestCodeBySms(@Body phone: PhoneRequest): Completable

    @POST("auth/validate")
    fun validateSmsCode(@Body authData: AuthDataRequest): Single<Response<Void>>
}