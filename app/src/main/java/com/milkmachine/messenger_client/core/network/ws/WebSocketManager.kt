package com.milkmachine.messenger_client.core.network.ws

import com.milkmachine.messenger_client.BuildConfig
import com.milkmachine.messenger_client.core.utils.toJson
import com.milkmachine.rxjava2interop.toV2Observable
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import org.java_websocket.WebSocket
import ua.naiksoftware.stomp.Stomp
import ua.naiksoftware.stomp.client.StompClient
import ua.naiksoftware.stomp.client.StompMessage

/**
 * Created by kefir on 09.03.2017.
 */
class WebSocketManager {

    private val stompClient: StompClient

    init {
        stompClient = Stomp.over(WebSocket::class.java, WEB_SOCKET_ENDPOINT)
    }

    companion object {
        private const val WEB_SOCKET_ENDPOINT = "wss://${BuildConfig.SERVER_ADDRESS}/ws/websocket"
    }

    fun getChatObservable(): Observable<StompMessage> {
        connect()
        return stompClient.topic("/chat/messages").toV2Observable()
    }

    fun <T : Any> sendMessage(any: T) {
        stompClient.send("/chat", any.toJson())
                .toV2Observable()
                .subscribeBy(onError = Throwable::printStackTrace)
    }

    fun unsubscribeFromChat() = disconnect()

    private fun connect() {
        if (!stompClient.isConnected) stompClient.connect()
    }

    private fun disconnect() = stompClient.disconnect()
}