package com.milkmachine.messenger_client.core.network.requests

/**
 * Created by kefir on 07.03.2017.
 */
data class AuthDataRequest(val phone: String, val code: String)