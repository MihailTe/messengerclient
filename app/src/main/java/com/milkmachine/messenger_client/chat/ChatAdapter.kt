package com.milkmachine.messenger_client.chat

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.milkmachine.messenger_client.R
import com.milkmachine.messenger_client.core.entitites.Message

/**
 * Created by kefir on 08.03.2017.
 */
class ChatAdapter(val userName: String) : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {

    companion object {
        private const val SENT_MESSAGE = 0
        private const val RECEIVED_MESSAGE = 1
    }

    private val messageList = mutableListOf<Message>()

    override fun getItemViewType(position: Int): Int {
        if (userName == messageList[position].username) return SENT_MESSAGE
        else return RECEIVED_MESSAGE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        @LayoutRes val layoutId: Int
        if (viewType == SENT_MESSAGE) layoutId = R.layout.sent_chat_item
        else layoutId = R.layout.received_chat_item

        return ViewHolder(LayoutInflater.from(parent.context).inflate(layoutId, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val message = messageList[position]

        holder.apply {
            username.text = message.username
            this.message.text = message.text
        }
    }

    fun addItem(message: Message) {
        messageList += message
        notifyItemInserted(itemCount)
    }

    fun getLastItemPosition() = itemCount.dec()

    override fun getItemCount(): Int = messageList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val username: TextView = itemView.findViewById(R.id.user_name) as TextView
        val message: TextView = itemView.findViewById(R.id.message) as TextView
    }
}