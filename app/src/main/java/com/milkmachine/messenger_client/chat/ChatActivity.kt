package com.milkmachine.messenger_client.chat

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatImageButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.EditText
import com.milkmachine.messenger_client.R
import com.milkmachine.messenger_client.auth.AuthActivity.Companion.USER_NAME
import com.milkmachine.messenger_client.core.DataManager
import com.milkmachine.messenger_client.core.entitites.Message
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy

class ChatActivity : AppCompatActivity() {

    private lateinit var messageList: RecyclerView
    private lateinit var chatAdapter: ChatAdapter
    private lateinit var sendButton: AppCompatImageButton
    private lateinit var messageEditText: EditText
    private lateinit var layoutManager: LinearLayoutManager

    private lateinit var userName: String
    private lateinit var chatDisposable: Disposable

    //region ============================== Lifecycle ==============================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        initUserName()
        setUpViews()
        initListeners()
        initRecyclerView()
    }

    override fun onResume() {
        super.onResume()
        subscribeOnChat()
    }

    override fun onPause() {
        chatDisposable.dispose()
        super.onPause()
    }

    //endregion

    private fun initListeners() {
        sendButton.setOnClickListener { sendMessage() }
    }

    private fun initUserName() {
        userName = intent.getStringExtra(USER_NAME).takeIf(String::isNotEmpty) ?: "Anonymous"
    }

    private fun setUpViews() {
        messageList = findViewById(R.id.chat_recycler_view) as RecyclerView
        sendButton = findViewById(R.id.send_button) as AppCompatImageButton
        messageEditText = findViewById(R.id.message_edit_text) as EditText
    }

    private fun sendMessage() {
        val messageText = messageEditText.text.toString()
        if (messageText.isEmpty()) return

        messageEditText.setText("")

        val message = Message(messageText, userName)

        DataManager.sendMessage(message)
    }

    private fun initRecyclerView() {
        chatAdapter = ChatAdapter(userName)
        layoutManager = LinearLayoutManager(this)

        messageList.layoutManager = layoutManager
        messageList.adapter = chatAdapter
    }

    private fun subscribeOnChat() {
        chatDisposable = DataManager.getChatObservable()
                .doOnDispose { DataManager.onUnsubscribeFromChat() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = this::addItemToRecyclerViewWithScroll,
                        onError = DataManager::onFatalError)
    }

    private fun addItemToRecyclerViewWithScroll(message: Message) {
        chatAdapter.addItem(message)
        layoutManager.scrollToPosition(chatAdapter.getLastItemPosition())
    }
}